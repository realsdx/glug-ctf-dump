#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>


char* concat(const char *s1, const char *s2)
{
    char *result = malloc(strlen(s1) + strlen(s2) + 1); // +1 for the null-terminator
    // in real code you would check for errors in malloc here
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}


char *doXor(char *string, const char *key)
{
    char *s = string;
    size_t length = strlen(key), i = 0;
    while (*s) {
            *s++ ^= key[i++ % length];
    }
    return string;
}

int cKey()
{
	int x=0x2,y=0x42;
	char *flag = "15_n07_53c1r17y";
	const char *key = "zsh";
	char *gs;
	if(x == 0x2 && (y+5) < 42)
	{
		gs = doXor(flag,key);
		return 0;
	}
	return 1;
}


char* getString(const char *s)
{
	// char fh[] = {'4', 'c', '7', 'u', '4', 'l', 'l', 'y', '_', 'n', '0', '7', '_', '7', 'h', '4', '7', '_', 'b', '4', 'd', 'x', 'D', '_', 'i', 't', 's', '_', 'a', 'c', 't', 'u', 'a', 'l', 'l', 'y', '_', 'g', 'o', 'o', 'd'};
	char fh[] = {'2', 'a', '5', 's', '2', 'j', 'j', 'w', ']', 'l', '.', '5', ']', '5', 'f', '2', '5', ']', '`', '2', 'b', 'v', 'B', ']', 'g', 'r', 'q', ']', '_', 'a', 'r', 's', '_', 'j', 'j', 'w', ']', 'e', 'm', 'm', 'b','{','\0'};
	char fg[] = {'7', 'f', ':', 'x', '7', 'o', 'o', '|', 'b', 'q', '3', ':', 'b', ':', 'k', '7', ':', 'b', 'e', '7', 'g', '{', 'G', 'b', 'l', 'w', 'v', 'b', 'd', 'f', 'w', 'x', 'd', 'o', 'o', '|', 'b', 'j', 'r', 'r', 'g','{','\0'};
 	//Some gib
	char fl[44];
	char fa[44];
	int i;
	for(i=0;i<43;i++)
	{
		fl[i] = fh[i]+2;
		fa[i] = fh[i]+1;
		fl[43] = '\0';
		fa[43] = '\0';
	}

	int x=0x7, j=0x23;
	if(j>x)
	{
		return concat(s,fl);
	}

	return NULL;
}

int flagHelper(const char *s)
{
	int i,j=0x4,k=0x10;
	if(j>= 0x46 && k > 0x41)
	{
		for(i=0;i<7;i++)
		{
			printf("The flag is:");
		}

		char *flag = getString(s);
		printf("\rHere is your FLAG: ");
		printf("%s",flag);

		if( flag != NULL)
			free(flag);
	}
	return 0;
}

int main()
{
	char fg[]={'G', 'L', 'U', 'G', '{','0', 'b', '5', '3', 'c', 'u', 'r', '1', '7', 'y', '_', '1', '5', '_','\0'};
	char c;
	puts("Do you want the flag?[y/N]");
	scanf("%c",&c);
	fflush(stdout);
	if(c == 'y')
	{
		int i,j=2;
		for(i=0;i<61;i++)
		{
			printf("%c",(0x41+i));
			fflush(stdout);
			usleep(20*1000);
			printf("\r");
		}
		printf("\nSome%}ing w%0red %~/ Tr%}0ps{ xs%$\n");
		flagHelper(fg);
		sleep(2);
		system("xdg-open https://www.youtube.com/watch?v=CEM5SXxpmlE");
	}
	else
	{
		system("xdg-open https://www.youtube.com/watch?v=75gBFiFtAb8");
	}
	return 0;
}