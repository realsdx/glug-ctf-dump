def xor(msg, key):
	return "".join([chr(ord(m)^ord(key)) for m in msg])


t = 'dovdX[LQ|JP|FUFQZTKFQF^'

s = "GLUG{xor_is_everywhere}"

for x in range(128):
    print("Key:",chr(x), xor(t,chr(x)))

# print(xor(t,'9'))

