def xor(msg, key):
	return "".join([chr(ord(m)^ord(key)) for m in msg])

def keyXor(msg, key):
	kl = len(key)
	res =[]
	for i,m in enumerate(msg):
		res.append(chr(ord(m)^ord(key[i%kl])))
	return "".join(res)


def ces(msg,shift):
	return "".join([chr(ord(m)+shift) for m in msg])


# msg = ['4', 'c', '7', 'u', '4', 'l', 'l', 'y', '_', 'n', '0', '7', '_', '7', 'h', '4', '7', '_', 'b', '4', 'd', 'x', 'D', '_', 'i', 't', 's', '_', 'a', 'c', 't', 'u', 'a', 'l', 'l', 'y', '_', 'g', 'o', 'o', 'd']

# x = ces(msg,-2)
# h = ces(msg,+3)
# print x
# print len(x)
# print list(x)
# print list(h)

flag = "GLUG{7h15_15_4l50_4_cl4551c}"
print len(flag)
x =keyXor(flag,"f")
print x
print repr(x)
print len(x)
