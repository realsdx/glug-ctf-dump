#include<stdio.h>
#include<stdlib.h>
#include <unistd.h>
 
int main(int argc, char const *argv[])
{
	if( argc < 2)
	{
		puts("Usage: do-this <command>");
		exit(1);
	}
	else
	{
		FILE *f;
	   char c;
	   f=fopen(argv[1],"rt");

	   while((c=fgetc(f))!=EOF){
	       printf("%c",c);
	   }
	   printf("\n");
	   fclose(f);
	   return 0;

	}
	return 0;
}
