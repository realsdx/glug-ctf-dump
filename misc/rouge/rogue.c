#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

void handler4(int sig) {
  FILE* fp = fopen("output.txt", "w");
  fprintf(fp, "Okay! You win. I am giving you the flag! It is GLUG{k1ll_7h3_51673rm}.\n");
  fprintf(fp, "Now leave me alone\n");
  fclose(fp);
  signal(SIGTERM, SIG_DFL);
  raise(SIGTERM);
}

void handler3(int sig) {
  FILE* fp = fopen("output.txt", "w");
  fprintf(fp, "Wait! What are you doing! Stop!!!\n");
  fclose(fp);
  signal(SIGTERM, handler4);
}

void handler2(int sig) {
  pid_t pid;
  FILE* fp = fopen("output.txt", "w");
  fprintf(fp, "Haahaa! I just got more powerful?\n");
  fclose(fp);
  if ((pid = fork()) == 0) {
    if ((pid = fork()) == 0) {
      if ((pid = fork()) == 0) {
        if ((pid = fork()) == 0) {
          signal(SIGTERM, handler3);
        } else {
          signal(SIGTERM, SIG_DFL);
        }
      } else {
      signal(SIGTERM, SIG_DFL);
      }
    } else {
    signal(SIGTERM, SIG_DFL);
    }
  } else {
    signal(SIGTERM, handler2);
  }
}

void handler1(int sig) {
  FILE* fp = fopen("output.txt", "w");
  fprintf(fp, "What did you think? I'd die so easily?\n");
  fclose(fp);
  signal(SIGTERM, handler2);
}

int main() {
  signal(SIGTERM, handler1);
  while(1) {
    sleep(1);
  }
  return 0;
}
