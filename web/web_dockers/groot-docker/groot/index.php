<?php
$cookie_name = "user";
$cookie_value = "unknown";
setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ctf</title>
    <style media="screen">
      body{
        background-image: url("background.jpg");
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        background-attachment: fixed;
        color:white;
      }
      .wrap{
        font-family: monospace;
        background:rgba(0,0,0,0.8);
        width: auto;
        padding: 7vh;
        margin-top: 10vh;
      }
    </style>
  </head>
  <body>

        <div class="wrap">
          <h1>I am gROOT!</h1><?php
        if(isset($_COOKIE["user"])&&$_COOKIE["user"]=="root") {
          echo "<h2>You can have it.<br>The password of my videogame is <h1 style='color:#00ffcc'>GLUG{I_@m_7r55t}</h1></h2>";
        }
        else echo "<h2>I don't share my videogame with anyone.<br>The password is with only me!</h2>";
        ?>
      </div>
      </div>
  </body>
</html>
