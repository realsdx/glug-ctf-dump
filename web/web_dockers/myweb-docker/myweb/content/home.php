<p>When I first started telling people, I was leaving teaching to pursue a career in computer programming, I
remember the first thing a fellow teacher said to me, “Are you good at that?” not “that’s awesome” or “let me
know how I can support” or “tell me more” but basically feeding my sense of self-doubt. When you have a dream,
you have to protect it, even from those who should be instilling confidence instead of doubt in others.</p>

<p>I remember my ex-partner telling me, when I was going to coding school from 8:30am — 6pm and taking on odd 
jobs like driving for LYFT, delivering Postmates, basically anything that would bring in income, to quit coding
school and go back to teaching (she was a teacher too). I had to protect my dream from even those closest to me
because, as Will Smith says in his film Pursuit of Happyness, “People can’t do something themselves, they wanna
tell you, ‘you can’t do it.’ Thankfully, I did not listen. I relied instead on my own guide — myself, as Will
Smith’s real life character Chris Gardner says, “If you want something. Go get it. Period”</p>


<p>I have been on the path to believing in myself since then. Honing my instincts, trusting myself and finding
others who believe / have faith in me — family, friends, co-workers, strangers. Letting go of people,
situations, and environments that does not feed the fire of belief in myself. I surround myself with people who
unequivocally believe in me, to the point of seeing what I cannot even see — my potential.</p>

<p>The amazing thing is that there are many more people who do believe in me and water the seeds of
self-confidence that has proven to be an antidote. I feel it in my bones everyday at work — people who have my
back and are cheering for me to succeed — including and most importantly — MYSELF. With my self-confidence
building come the fruits of my labor, I was recently asked to participate in a panel at Transform Tech. I’m
excited to share my journey with my trans and gender non-conforming community.</p>