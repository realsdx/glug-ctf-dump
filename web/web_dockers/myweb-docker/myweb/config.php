<?php

/**
 * Used to store website configuration information.
 *
 * @var string
 */
function config($key = '')
{
    $config = [
        'name' => 'My Personal Website',
        'nav_menu' => [
            '' => 'Home',
            'about-us.php' => 'About Us',
            'products.php' => 'Products',
            'contact.php' => 'Contact',
        ],
        'template_path' => 'template',
        'content_path' => 'content',
        'version' => 'v1.0',
    ];

    return isset($config[$key]) ? $config[$key] : null;
}
