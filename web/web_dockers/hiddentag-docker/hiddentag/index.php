<!DOCTYPE html>
<html>
<head>
	<title>Flag!Flag!Flag!</title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<div class='contain-flag'>
	  <div class='pole'></div>
	  <div class='flag'></div>
	  <div class='shadow'></div>
	  <div class='flag flag-2'></div>
	</div>
	<div>
		<center>
		<h3>This is just a decoy of the FLAG!</h3>
		<small>If you ask nicely, maybe i will give it to you.</small><br>
		<small>View Source Code of this page <a href="https://pastebin.com/LvfdATxz" target="_blank">Here</a></small>
		</center>
	</div>
	<pre><?php
	$flag = "GLUG{4lw4y5_fuzz_f0r_h1dd3n_p4r4m373r5}";

	if(array_key_exists("flag", $_GET)) {
	    if($_GET["flag"] === "true"){
	    echo $flag;
		}
	}
	?>
	</pre>
</body>
</html>