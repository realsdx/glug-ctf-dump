-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: Delegates
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu0.17.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Leaders`
--

DROP TABLE IF EXISTS `Leaders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Leaders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) NOT NULL,
  `Votes` int(11) NOT NULL,
  `IhidItHere` varchar(30) DEFAULT NULL,
  `Image` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Leaders`
--

LOCK TABLES `Leaders` WRITE;
/*!40000 ALTER TABLE `Leaders` DISABLE KEYS */;
INSERT INTO `Leaders` VALUES (1,'Thor',40,NULL,'https://cdn3.movieweb.com/i/article/wZcyipG8nbU6tXtU1sKOeAicPkI64x/798:50/Thor-Age-Avengers-Infinity-War.jpg'),(2,'Ironman',55,NULL,'https://www.sideshowtoy.com/assets/products/903341-iron-man-mark-iv/lg/marvel-iron-man-2-iron-man-mark-4-sixth-scale-figure-hot-toys-903340-08.jpg'),(3,'Hulk',5,NULL,'https://thumbor.forbes.com/thumbor/1280x868/https%3A%2F%2Fblogs-images.forbes.com%2Frobcain%2Ffiles%2F2017%2F07%2FHulk.jpeg'),(4,'Black widow',75,NULL,'https://static.independent.co.uk/s3fs-public/thumbnails/image/2018/01/11/09/black-widow-2.jpg?w968h681'),(5,'Captain America',40,NULL,'https://www.sideshowtoy.com/assets/products/9034301-captain-america-movie-promo-edition/lg/marvel-avengers-infinity-war-captain-america-movie-promo-sixth-scale-figure-hot-toys-9034301-01.jpg'),(6,'Dr. Strange',56,NULL,'https://cdn.vox-cdn.com/thumbor/aLr_sN03sYyb0hF-mwoBc0DSyhs=/0x0:1500x750/1200x800/filters:focal(762x94:1002x334)/cdn.vox-cdn.com/uploads/chorus_image/image/51717779/strange.0.jpg'),(7,'Spiderman',61,NULL,'http://media.comicbook.com/2018/04/spider-man-1099203.jpeg'),(8,'Vision',30,NULL,'https://i.ytimg.com/vi/KiwlOQseLOY/maxresdefault.jpg'),(9,'Loki',0,'Glug{3@st_!r0nm@n_1s_3est}','https://imgix.bustle.com/rehost/2016/9/13/866f0417-c13c-4a36-bbce-d860f800363d.jpeg?w=970&h=582&fit=crop&crop=faces&auto=format&q=70'),(10,'Gamora',32,NULL,'https://am21.akamaized.net/tms/cnt/uploads/2018/04/guardians-of-the-galaxy-gamora-1200x675.jpg');
/*!40000 ALTER TABLE `Leaders` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-27  0:56:55
