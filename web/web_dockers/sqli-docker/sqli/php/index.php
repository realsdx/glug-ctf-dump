<!DOCTYPE html>
<html>
   <head>
        <meta charset="utf-8">
       <title>CTF</title>
       <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
       <link href="https://fonts.googleapis.com/css?family=Shojumaru" rel="stylesheet">
       <style media="screen">
         body{
           background-color: #000d1a;

         }
         h1{
           font-family: 'Shojumaru', cursive;
           text-shadow:0px 10px 15px #00ecff;
           color:white;
         }
         h6{
           font-family: sans-serif;
         }
         .card{
           box-shadow: 0px 5px 10px #00ecff;
         }
       </style>
   </head>
   <body>
     <div class="container">

     <?php
     phpinfo();

     if(isset($_POST["valueToSearch"])&&$_POST["valueToSearch"]!="")
     {
        //$Search = ;
        // Search in all table columns
        // using concat mysql function
       // echo "yay"
       $query = "SELECT id,Name,Votes,Image FROM Leaders WHERE name = '$_POST[valueToSearch]' ";#12345
        //$query = "SELECT id,Name,Votes FROM authors WHERE CONCAT(first_name,last_name,id,birthdate,email) LIKE '%$_POST[valueToSearch]%' ";#12345
        echo $query;
        $search_result = filterTable($query);
     }
     else {
        $query = "SELECT id,Name,Votes,Image FROM Leaders";
        $search_result = filterTable($query);
     }

     // function to connect and execute the query
     function filterTable($query)
     {
       $servername = "db";
       $username = "guest";
       $password = "guest";
       $dbname = "Delegates";

       // Create connection

       $conn = mysqli_connect($servername, $username, $password,$dbname);
       if (!$conn) {
           echo "ERROR: Unable to connect to MySQL." . PHP_EOL . '<br>';
           echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL . '<br>';
           echo "Debugging error: " . mysqli_connect_error() . PHP_EOL . '<br>';
           exit;
       }

       // Check connection
       if ($conn->connect_error) {
         die("Connection failed: " . $conn->connect_error);
       }
       //echo "Connected successfully";
        $filter_Result = mysqli_query($conn, $query);
        return $filter_Result;
     }

     ?>
        <h1 class="font-weight-bold text-center m-5">THE RANGER POLITICS</h1>
        <div class="row flex-wrap justify-content-center mt-3">
          <form method="post">
           <input name="valueToSearch" placeholder="Enter name" type="text">
           <input type="submit" name="search" value="Filter"><br><br>
            </form>
        </div>


           <div class="row flex-wrap justify-content-center">
               <?php while($row = mysqli_fetch_array($search_result)):?>
                 <div class="card m-3 bg-light border-0" style="width: 18rem;" >
                   <?php echo "<img class='card-img-top' src='$row[3]' alt='Image not available' height='250px' weight='200px'>";?>
                   <div class="card-body ml-2 p-0">
                     <p class="card-text">
                       <h6 class="m-0 p-0 font-weight-bold">Id: <?php echo $row[0];?></h6>
                       <h6 class="m-0 p-0 font-weight-bold">Name: <?php echo $row[1];?></h6>
                       <h6 class="m-0 p-0 font-weight-bold">Votes: <?php echo $row[2];?></h5></p>
                   </div>
                 </div>
               <?php endwhile;?>
           </div>

         </div>
   </body>
</html>
