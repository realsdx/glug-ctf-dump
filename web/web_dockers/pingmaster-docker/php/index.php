<!DOCTYPE html>
<html>
<head>
	<title>PingMaster | by Phonix</title>
	<style type="text/css">
		* {
		  font-family: "Lato", sans-serif;
		}
		*:focus {
		  outline: none;
		}
		body {
		  background: white;
		  color: #A9A9A9;
		}
		#txtSearch {
		  -webkit-appearance: none;
		  margin-top: 30px;
		  box-sizing: border-box;
		  padding-left: 10px;
		  border-radius: 40px;
		  font-size: 14px;
		  border: 3px solid #00B8FF;
		  color: #00B8FF;
		  background: none;
		  height: 40px;
		  width: 500px;
		  margin: 0;
		  transition: 0.3s;
		  box-shadow: inset 0 0 0 none;
		}
		::-webkit-input-placeholder {
		  color: lightblue; 
		}
		#txtSearch:hover {
		  color: #fff;
		  box-sizing: border-box;
		  border: 3px solid #00B8FF;
		  background: #00B8FF;
		}
		#txtSearch:focus {
		  background: #00B8FF;
		  border: 3px solid #00B8FF;
		  color: #fff
		}
		#btnSearch {
		  font-family: "Lato";
		  -webkit-appearance: none;
		  border-radius: 30px;
		  height: 40px;
		  width: 70px;
		  color: white;
		  background: #00B8FF;
		  border: none;
		  margin-left: 30px;
		  cursor: pointer;
		  transition-duration: 0.3s;
		}
		#btnSearch:hover {
		  background: none;
		  color: #00B8FF;
		  border: 3px solid #00B8FF;
		}
		#btnSearch:active {
		  border-color: #0086C6;
		  background: none;
		}
		.input-container {
		  display: flex;
		  margin-top: 30px;
		  padding: 0;
		  margin-left: 30px;
		  text-align: center;
		}
		#dis {
		  margin-top: 30px;
		  display: block;
		  text-align: justify;
		  margin-left: 30px;
		  width: 600px;
		  color: #0E4CF1;
		  font-size: 1.5rem;
		  text-decoration: underline;
		}
		.heading {
			color: #00B8FF;
			margin-left: 30px;	
		}
		.output {
			margin-left: 30px;
			text-align: justify;
			color: #030341;
		}
	</style>
</head>
<body>
	<h2 class="heading">Ping Master</h2><small class="heading" style="color: red;">by Phonix</small><br>
	<div class="input-container">
		<form method="get">
			<input type="text" id="txtSearch" placeholder="Host to Ping" spellcheck="false" name="host"/>
			<button id="btnSearch" type="submit"> Ping</button>
		</form>		
	</div>
	<br>
	<span id="dis">Output</span>
	<br>
	<div class="output">
	    <pre><?php
	    $host = "";

	    if(array_key_exists("host", $_GET)) {
	        $host = $_GET["host"];
	        // print "DB ";
	        // echo $host."\n";
	        // print_r($_GET);
	    }

	    if($host != "") {
	        if(preg_match('/[;|& ]/',$host)) {
	            print "Input contains an illegal character!";
	        } else {
	            passthru("ping -c 4 $host");
	        }
	    }
	    ?>
	    </pre>
	</div>
</body>
</html>