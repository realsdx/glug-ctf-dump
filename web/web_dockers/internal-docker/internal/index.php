<!DOCTYPE html>
<html>
<head>
	<title>Internal Mimikatz MIssion control</title>
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<div class="terminal-window">
	  <header>
	    <div class="button green"></div>
	    <div class="button yellow"></div>
	    <div class="button red"></div>
	  </header>
	  <section class="terminal">
	    <div class="history"></div>
	    $&nbsp;<span class="prompt"></span>
	    <span class="typed-cursor"></span>
	    
	  </section>
	</div>
	<pre>
		<?php
		$the_ip = "";
		$check = '<input class="check-admin" type="hidden" value="0"/>';
		$flag = "Unauthorized access detected! This incident will be reported";
		if ( array_key_exists( 'X-Forwarded-For', $_SERVER ) && filter_var( $_SERVER['X-Forwarded-For'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
		    $the_ip = $_SERVER['X-Forwarded-For'];
		}
		elseif ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $_SERVER ) && filter_var( $_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) ) {
		  $the_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}

		if($the_ip !== "") {
			if($the_ip === "127.0.0.1") {
				$check = '<input class="check-admin" type="hidden" value="1"/>';
				$flag = "Welcome Admin!, Your flag is GLUG{d0n7_7ru57_4ny7h1n6_fr0m_cl13n7_51d3}";
			}
		}
		echo $check;
		?>
	</pre>
	<input class="check-admin" type="hidden" value="0"/>
	  <div class="terminal-data mimikatz-run-output">
	 <br>Found 0 flag<br>
	 ----------------------------------------------<br>
	 Mission: Keep Flag  <span class="gray"># ./secrets/admin.key</span><br><br> 

	 &nbsp;&nbsp;Mimikatz Mission Control: For internal use<br>
	    &nbsp;&nbsp;&nbsp;&nbsp;<span class="green">✓</span> <span class="gray">Username is admin</span><br>
	 &nbsp;&nbsp;&nbsp;&nbsp;<span class="green">✓</span> <span class="gray">Request is sent using our custom terminal</span><br>
	 &nbsp;&nbsp;&nbsp;&nbsp;<span class="red">✗</span> <span class="gray">Request form a unauthorized external network</span><br>
	 <br>
	    <span class="gray">&nbsp;---------- -----------</span><br>
	    &nbsp;&nbsp;Status&nbsp;&nbsp;&nbsp;<span class="red">Failed</span><br>
	    <span class="gray">&nbsp;---------- -----------</span><br>
	  <br>
	&nbsp;&nbsp;<pre><?php echo $flag;?></pre><br>
	  <br>
	</div>

	  <div class="terminal-data mimikatz-run-output-flag">
	 <br>Found 1 flag<br>
	 ----------------------------------------------<br>
	 Mission: Keep Flag  <span class="gray"># ./secrets/admin.key</span><br><br> 

	 &nbsp;&nbsp;Mimikatz Mission Control: For internal use<br>
	    &nbsp;&nbsp;&nbsp;&nbsp;<span class="green">✓</span> <span class="gray">Username is admin</span><br>
	 &nbsp;&nbsp;&nbsp;&nbsp;<span class="green">✓</span> <span class="gray">Request is sent using our custom terminal</span><br>
	 &nbsp;&nbsp;&nbsp;&nbsp;<span class="green">✓</span> <span class="gray">Request form our Internal network</span><br>
	 <br>
	    <span class="gray">&nbsp;---------- ----------- </span><br>
	&nbsp;&nbsp;Status&nbsp;&nbsp;&nbsp;<span class="green">Success</span><br>
	    <span class="gray">&nbsp;---------- -----------</span><br>
	  <br>
	&nbsp;&nbsp;<pre><?php echo $flag;?></pre><br>
	  <br>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://www.mattboldt.com/demos/typed-js/js/typed.custom.js"></script>
	<script src="js/main.js"></script>
</body>
</html>