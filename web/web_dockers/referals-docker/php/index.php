<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ctf</title>
    <style media="screen">
      body{
        background-color:#f2f2f2;
      }
      .wrap{
        font-family: monospace;
      }
    </style>
  </head>
  <body>
        <div class="wrap"><?php
        if(isset($_SERVER['HTTP_REFERER'])&&$_SERVER['HTTP_REFERER']=='https://www.google.com') {
          echo "<h2 class='message'>The flag is <span style='color:green'>GLUG{Refendrum_@_007}</span></h2>";
        }
        else echo "<h2 class='message'>Sorry,But are you coming here from <a href='https://www.google.com' target='_blank'>www.google.com</a>??</h2>";
        ?>
      </div>
      </div>
  </body>
</html>
